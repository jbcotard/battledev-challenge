#*******
#* Read input from STDIN
#* Use print to output your result to STDOUT.
#* Use sys.stderr.write() to display debugging information to STDERR
#* ***/
import sys

lines = []
for line in sys.stdin:
	lines.append(line.rstrip('\n'))
	
listeComptes = [x for x in lines if len(x[-5:]) == 5 and x[-5:].isdigit() ]

for i in lines:
    sys.stderr.write(i + '\n')
    sys.stderr.write(i[-5:] + '\n')

print(len(listeComptes))