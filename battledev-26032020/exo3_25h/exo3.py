#*******
#* Read input from STDIN
#* Use: echo or print to output your result to STDOUT, use the /n constant at the end of each result line.
#* Use: sys.stderr.write() to display debugging information to STDERR
#* ***/
import sys
from datetime import datetime, timedelta


def get_slots(day, hours, appointments, duration=timedelta(minutes=59)):
    slots = sorted([(hours[0], hours[0])] + appointments + [(hours[1], hours[1])])
    for start, end in ((slots[i][1], slots[i+1][0]) for i in range(len(slots)-1)):
        #assert start < end, "Cannot attend all appointments"
        while start + duration < end:
            print("{} {:%H:%M}-{:%H:%M}".format(day, start, start + duration))
            start += duration
            
            
n = int(input())
lines = []
for line in sys.stdin:
	lines.append(line.rstrip('\n'))

#creneaux_impossibles=[]
jours=[]
creneaux=[]
for line in lines:
    jours.append(int(line.split(' ')[0]))
    creneaux.append(line.split(' ')[1])
    
#    creneaux_impossibles.append({'j':line.split(' ')[0], 'h':line.split(' ')[1]})
year=2020
mois=3

# pour gerer les jours ou il y a aucune indispo
tj=[1,2,3,4,5]
for j in tj:
    if j not in jours:
        print(f"{j} 08:00-08:59")
    else:
        hours = (datetime(year, mois, j, 8), datetime(year, mois, j, 18))
        appointments = []
        liste_cr_j=[ item[1] for item in zip(jours,creneaux) if item[0] == j]
        for cr in liste_cr_j:
            deb=cr.split('-')[0]
            fin=cr.split('-')[1]
            fin_h=int(fin.split(':')[0])
            fin_m=int(fin.split(':')[1])
            if fin_m == 59:
                fin_h+=1
                fin_m=0
            else:
                fin_m+=1
            appointments.append((datetime(year, mois, j, int(deb.split(':')[0]),int(deb.split(':')[1])), datetime(year, mois, j, fin_h,fin_m)))
        
        get_slots(j, hours, appointments)

            
        

# on regarde les jours ou il y le moins d'inspo
#jour_min_indispo=min(jours)
#liste_cr_j_min=[ item[1] for item in zip(jours,creneaux) if item[0] == jour_min_indispo]

#for cr in liste_cr_j_min:
#    cr.split('-')
#for j_item, cr_item in zip(jours,creneaux):
#    if j_item == jour_min_indispo:


print(f"2 08:00-08:59")
#print(f"3 08:00-08:59")
#print(f"{jour_min_indispo} 11:23-12:22")

        