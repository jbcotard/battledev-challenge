#*******
#* Read input from STDIN
#* Use: echo or print to output your result to STDOUT, use the /n constant at the end of each result line.
#* Use: sys.stderr.write() to display debugging information to STDERR
#* ***/
import sys


n = int(input())
lines = []
for line in sys.stdin:
	lines.append(line.rstrip('\n'))

first=""
nb_first=0
second=""
nb_second=0
	
for color in lines:
    nb=lines.count(color)
    if nb > nb_first and color != first and color != second :
        second=first
        nb_second=nb_first
        first=color
        nb_first=nb
    elif nb > nb_second and color != first and color != second :
        second=color
        nb_second=nb
        
print(f"{first} {second}")