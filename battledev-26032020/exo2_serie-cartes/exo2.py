#*******
#* Read input from STDIN
#* Use: echo or print to output your result to STDOUT, use the /n constant at the end of each result line.
#* Use: sys.stderr.write() to display debugging information to STDERR
#* ***/
import sys

n = int(input())
lines = []
for line in sys.stdin:
	lines.append(line.rstrip('\n'))
	
num=0
nb_serie=0
cur_num=0
cur_nb_serie=0

for numero in lines:
    if cur_num == int(numero):
        cur_nb_serie+=1
    else : 
        if num != cur_num and cur_nb_serie >= nb_serie:
            num=cur_num
            nb_serie=cur_nb_serie
        cur_num=int(numero)
        cur_nb_serie=1

# pour le dernier element
if cur_nb_serie >= nb_serie:
    nb_serie=cur_nb_serie
        
print(nb_serie) 